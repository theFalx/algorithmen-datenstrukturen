package sort;

import java.util.Arrays;

public class Util 
{
	public static <E> void swap( E[] arr, int i, int j )
	{
		E tmp  = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}
	
	public static <E> void print( Class<?> clazz, E[] arr )
	{
		System.out.printf( "Sorted with %s:\n\t%s\n\n", clazz.getName(), Arrays.toString( arr ) );
	}
	
	public static <E> int fill( E[] target, int k, E[] source, int i )
	{
		for( ; i < source.length; i++ )
			target[k++] = source[i];
		
		return( k );
	}

}
