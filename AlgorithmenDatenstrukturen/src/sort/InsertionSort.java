package sort;



public class InsertionSort 
{
	
	public static < E extends Comparable<E> > E[] sort( E[] arr )
	{
		for( int i = 1; i < arr.length; i++ )
		{
			E elem = arr[i];
			
			int j;
			for( j = i; j >= 1; j-- )
			{
				if( elem.compareTo( arr[j-1] ) < 0 )
					arr[j] = arr[j-1];
				else
					break;
			}
			
			arr[j] = elem;
		}
		
		return( arr );
	}
	
}
