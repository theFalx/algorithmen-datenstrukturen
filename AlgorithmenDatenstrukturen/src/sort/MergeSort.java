package sort;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MergeSort 
{
	
	public static <E extends Comparable<E> > E[] sort( E[] arr, Class<?> clazz )
	{
		if( arr.length == 1 )
			return( arr );
		else
		{
			int half 	= arr.length / 2;
			E[] partOne =  Arrays.copyOfRange( arr, 0, half);
			E[] partTwo =  Arrays.copyOfRange( arr, half, arr.length );

			sort( partOne, clazz );
			sort( partTwo, clazz );
			
			return( merge( partOne, partTwo, clazz ) );
		}
	}
	
	
	public static < E extends Comparable<E> > E[] merge( E[] partOne, E[] partTwo, Class<?> clazz )
	{
		@SuppressWarnings("unchecked")
		E[] ret = (E[])Array.newInstance( clazz, partOne.length + partTwo.length );
		
		int k = 0; // index for the new array
		int i = 0; // index for parteOne
		int j = 0; // index for partTwo
		
		while( i < partOne.length && j < partTwo.length )
		{
			if( partOne[i].compareTo( partTwo[j] ) < 0 )
				ret[k++] = partOne[i++];
			else
				ret[k++] = partTwo[j++];
		}
		
		if( i < partOne.length )
			k = Util.fill( ret, k, partOne, i );
		
		if( j < partTwo.length )
			k = Util.fill( ret, k, partTwo, j );
		
		return( ret );
	}
}
