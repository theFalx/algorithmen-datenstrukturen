package sort;

public class BubbleSort 
{
	// Optimierter Algorithmus! Da nach dem ersten Durchlauf das größte Element schon richtig eingeordnet ist
	// muss man anschließend dieses nicht nochmal betrachten. Hierfür wird die obere Bereichsgrenze nach jedem
	// Durchlauf um eins herabgesetzt. 
	// WICHTIG: Somit stimmt die Aufwandsberechnung aus der Vorlesung  N I C H T mehr! Der Aufwand ist aber nach
	// nach wie vor aus der Komplexitätsklasse O( n ^2 )!
	public static < E extends Comparable<E> > E[] sort( E[] arr )
	{
		boolean swapped  = false;
		int 	j		 = arr.length - 1 ;
		
		do
		{
			swapped = false;
			
			for( int i = 0; i < j; i++ )
			{
				if( arr[i].compareTo( arr[i+1] ) > 0 )
				{
					Util.swap( arr, i, i+1 );
					swapped = true;
				}
			}
			
		}while( swapped );
		
		return( arr );
	}
	
}
