package sort;

public class SelectionSort 
{
	
	public static < E extends Comparable<E> > E[] sort( E[] arr )
	{
		for( int i = 0; i < arr.length - 1; i++ )
		{
			E element = arr[i];
			int p = i;
			
			for( int j = i + 1; j < arr.length; j++ )
			{
				if( element.compareTo( arr[j] ) > 0 )
					p = j;
			}
			
			if( p != i )
				Util.swap( arr, p, i );
		}
		
		return( arr );
	}
	
}
