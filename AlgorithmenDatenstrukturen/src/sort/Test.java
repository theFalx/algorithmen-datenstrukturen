package sort;

import java.util.Arrays;

public class Test 
{
	public static void main( String[] args )
	{
		Integer[] toSort = { 31, 26, 1, 7, 15, 11, 45, 33, 9, 80, 65, 55 };
		System.out.printf( "Unsorted array:\n\t%s\n\n", Arrays.toString( toSort ) );
		
		Integer[] arr = InsertionSort.sort( toSort );
		Util.print( InsertionSort.class, arr );
		
		arr = SelectionSort.sort( toSort );
		Util.print( SelectionSort.class, arr );
		
		arr = BubbleSort.sort( toSort );
		Util.print( BubbleSort.class, arr );
		
		arr = MergeSort.sort( toSort, Integer.class );
		Util.print( MergeSort.class, arr );
	}
}
