package tree;

import java.util.Stack;

public class BinTree 
{
	public static void main( String[] args )
	{
		TreeNode node = new TreeNode( 11 );
		node.insert( 2 );
		node.insert( 15 );
		node.insert( 1 );
		node.insert( 6 );
		node.insert( 12 );
		node.insert( 18 );
		node.insert( 4 );
		node.insert( 10 );
		node.insert( 8 );
		
		node.print();
		System.out.println();
		
		System.out.println( node.search( 6 ).key );
		System.out.println( node.search( -1 ) );
		System.out.println( node.minChild().key );
		System.out.println( node.maxChild().key );
	}
	
	
	static class TreeNode 
	{
		int key;
		
		TreeNode parent = null;
		TreeNode left   = null;
		TreeNode right  = null;
		
		public TreeNode( int k )
		{
			key = k;
		}
		
		public void setLeft( TreeNode n )
		{
			left = n;
			n.parent = this;
		}
		
		public void setRight( TreeNode n )
		{
			right = n; 
			n.parent = this;
		}
		
		public TreeNode insert( int k )
		{
			TreeNode node = this;
			while( node != null )
			{
				if( k > node.key )
				{
					if( node.right != null )
						node = node.right;
					else
					{
						TreeNode newNode = new TreeNode( k );
						node.setRight( newNode );
						return( newNode );
					}
				}
				else if( k < node.key )
				{
					if( node.left != null )
						node = node.left;
					else
					{
						TreeNode newNode = new TreeNode( k );
						node.setLeft( newNode );
						return( newNode );
					}
				}
					
			}
			return( null );
		}
		
		
		public TreeNode minChild()
		{
			TreeNode ret  = this;
			TreeNode node = this.left;
			
			while( node != null )
			{
				ret = node;
				node = node.left;
			}
			
			return( ret );
		}
		
		
		public TreeNode maxChild()
		{
			TreeNode ret  = this;
			TreeNode node = this.right;
			
			while( node != null )
			{
				ret = node;
				node = node.right;
			}
			
			return( ret );
		}
		
		public TreeNode pred()
		{
			return( parent );
		}
		
		
		
		public void print()
		{
			Stack<TreeNode> stack = new Stack<>();
			TreeNode		node  = this;
			
			while( !stack.isEmpty() || node != null )
			{
				while( node != null )
				{
					stack.push( node );
					node = node.left;
				}
				
				node = stack.pop();
				System.out.print( " " + node.key );
				node = node.right;
			}
		}
		
		
		public TreeNode search( int k )
		{
			if( this.key == k )
				return( this );
			else
			{
				if( k < this.key && this.left != null )
					return( this.left.search( k ) );
				else if( k > this.key && this.right != null )
					return( this.right.search( k ) );
				else
					return( null );
			}
		}
	}
}
